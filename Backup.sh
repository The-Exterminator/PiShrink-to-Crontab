#!/bin/bash

# Settings
BACKUP_PATH="/YOU/BACKUP/FOLDER"
BACKUP_ANTAL="3"
BACKUP_NAME="MagicMirror"
BACKUP_DATO=$(date +%d-%m-%Y)
SERVICE_START_STOP="service  pm2-pi"

# Stop SERVICE for Backup
${SERVICE_START_STOP} stop

# Backup Image
dd if=/dev/mmcblk0 of=${BACKUP_PATH}/${BACKUP_NAME}.${BACKUP_DATO}.img bs=1MB

# Start SERVICE for Backup
${SERVICE_START_STOP} start

# PiShrink
pishrink.sh ${BACKUP_PATH}/${BACKUP_NAME}.${BACKUP_DATO}.img

# Delete if there are more than ${BACKUP_ANTAL}
pushd ${BACKUP_PATH}; ls -tr ${BACKUP_PATH}/${BACKUP_NAME}* | head -n -${BACKUP_ANTAL} | xargs rm; popd
